//
//  WebPic.m
//  PicsLoader
//
//  Created by admin on 25.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "WebPic.h"

@implementation WebPic

- (id)initWithName:(NSString *)name URL:(NSURL *)URL
{
    if (self = [super init]) {
        _name = name;
        _URL = URL;
    }
    return self;
}

- (BOOL)hasImage
{
    return _image != nil;
}

- (BOOL)isFailed
{
    return _failed;
}

@end
