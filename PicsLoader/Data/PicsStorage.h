//
//  PicsStorage.h
//  PicsLoader
//
//  Created by admin on 20.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WebPic;

@interface PicsStorage : NSObject

@property (nonatomic, readonly) NSArray *pics;

- (void)requestDownloadingPicsForCurrentTime;
- (void)requestRetryDownloadingPic:(WebPic *)webPic;

@end
