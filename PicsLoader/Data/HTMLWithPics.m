//
//  HTMLWithPics.m
//  PicsLoader
//
//  Created by admin on 25.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "HTMLWithPics.h"
#import "WebPic.h"

@implementation HTMLWithPics

- (id)initWithURL:(NSURL *)URL
{
    if (self = [super init]) {
        _URL = URL;
    }
    return self;
}

- (BOOL)hasSource
{
    return _source != nil;
}

- (BOOL)isFailed
{
    return _failed;
}

// parse source and return array of WebPic objects
- (NSArray *)pics
{
    if (self.isFailed || !self.hasSource) {
        return @[];
    }
    
    static NSUInteger defaultPicsCount = 200;
    
    NSMutableArray *pics = [NSMutableArray arrayWithCapacity:defaultPicsCount];
    
    // simple and dumb way:
    // all image URLs are between occurences of 
    // 'alt="[IMG]"></td><td><a href="' and '">'
    static NSString *searchLeftText = @"alt=\"[IMG]\"></td><td><a href=\"";
    static NSString *searchRightText = @"\">";
    
    NSRange searchLeftRange = NSMakeRange(0, _source.length);
    NSRange foundLeftRange;
    while (searchLeftRange.location < _source.length) {
        searchLeftRange.length = _source.length - searchLeftRange.location;
        foundLeftRange = [_source rangeOfString:searchLeftText options:NSCaseInsensitiveSearch range:searchLeftRange];
        if (NSNotFound == foundLeftRange.location) {
            // no more substring to find
            break;
        } else {
            NSRange searchRightRange = NSMakeRange(foundLeftRange.location + foundLeftRange.length, _source.length - foundLeftRange.location - foundLeftRange.length);
            NSRange foundRightRange = [_source rangeOfString:searchRightText options:NSCaseInsensitiveSearch range:searchRightRange];
            if (NSNotFound == foundRightRange.location) {
                // no more substring to find
                break;
            } else {
                // now get pic URL
                NSRange picURLRange = NSMakeRange(foundLeftRange.location + foundLeftRange.length, foundRightRange.location - foundLeftRange.location - foundLeftRange.length);
                NSString *picName = [_source substringWithRange:picURLRange];
                NSURL *absURL = [self.URL URLByAppendingPathComponent:picName];
                
                WebPic *webPic = [[WebPic alloc] initWithName:picName URL:absURL];
                [pics addObject:webPic];
                
                searchLeftRange.location = foundRightRange.location + foundRightRange.length;
            }
        }
    }
    return pics;
}

@end
