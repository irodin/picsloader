//
//  PicsStorage.m
//  PicsLoader
//
//  Created by admin on 20.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "PicsStorage.h"
#import "AppConstants.h"
#import "DownloadsManager.h"
#import "HTMLWithPics.h"
#import "WebPic.h"

@interface PicsStorage () {
    NSMutableDictionary *_picsByNames; // use dictionary for fast search by pic name
}

@end

@implementation PicsStorage 

@dynamic pics;

- (id)init
{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onHTMLDidLoaded:) name:PLHTMLDidLoadNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPicDidLoaded:) name:PLPicDidLoadNotification object:nil];
        
        _picsByNames = [NSMutableDictionary new];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)requestDownloadingPicsForCurrentTime
{
    [self requestDownloadingPicsForDate:[NSDate date]];
}

- (void)requestDownloadingPicsForDate:(NSDate *)aDate
{
    [[DownloadsManager sharedInstance] downloadHTMLWithPicsForDate:aDate];
}

- (void)requestRetryDownloadingPic:(WebPic *)webPic
{
    if (nil != webPic) {
        [[DownloadsManager sharedInstance] downloadPics:@[webPic]];
    }
}

- (void)onHTMLDidLoaded:(NSNotification *)aNotification
{
    HTMLWithPics *html = (HTMLWithPics *)aNotification.object;
    
    NSArray *webPics = [html pics];
    
    [_picsByNames removeAllObjects];
    for (WebPic *webPic in webPics) {
        [_picsByNames setObject:webPic forKey:webPic.name];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:PLHTMLDidParseNotification object:nil];
    
    if (0 == _picsByNames.count) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[UIAlertView alloc] initWithTitle:@"" message:@"No pics to download!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        });
    } else {
        [[DownloadsManager sharedInstance] downloadPics:webPics];
    }
}

- (void)onPicDidLoaded:(NSNotification *)aNotification
{
    WebPic *webPic = (WebPic *)aNotification.object;
    
    [_picsByNames setObject:webPic forKey:webPic.name];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:PLPicDidUpdateNotification object:webPic];
}

- (NSArray *)pics
{
    return [_picsByNames allValues]; // caution: order is not guaranteed
}

@end
