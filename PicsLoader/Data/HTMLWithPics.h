//
//  HTMLWithPics.h
//  PicsLoader
//
//  Created by admin on 25.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTMLWithPics : NSObject

@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, strong) NSString *source;
@property (nonatomic, readonly) BOOL hasSource; // Return YES if HTML is downloaded.
@property (nonatomic, getter = isFailed) BOOL failed; // Return Yes if HTML failed to be downloaded

- (id)initWithURL:(NSURL *)URL;
- (NSArray *)pics;

@end
