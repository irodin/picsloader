//
//  WebPic.h
//  PicsLoader
//
//  Created by admin on 25.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebPic : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, readonly) BOOL hasImage; // Return YES if image is downloaded.
@property (nonatomic, getter = isFailed) BOOL failed; // Return Yes if image failed to be downloaded

- (id)initWithName:(NSString *)name URL:(NSURL *)URL;

@end
