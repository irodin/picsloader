//
//  GalleryViewController.m
//  PicsLoader
//
//  Created by admin on 19.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "GalleryViewController.h"
#import "AppConstants.h"
#import "PicsStorage.h"
#import "PicCell.h"
#import "WebPic.h"

@interface GalleryViewController () {
    PicsStorage *_storage;
}

@end

@implementation GalleryViewController {
    UIAlertView *_loadingAlertView;
}

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    _storage = [PicsStorage new];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onHTMLDidLoaded:) name:PLHTMLDidLoadNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onHTMLDidParse:) name:PLHTMLDidParseNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    _loadingAlertView = [[UIAlertView alloc] initWithTitle:@"Loading pictures list\nPlease Wait..." message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    [_loadingAlertView show];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    indicator.center = CGPointMake(_loadingAlertView.bounds.size.width / 2, _loadingAlertView.bounds.size.height - 50);
    [indicator startAnimating];
    [_loadingAlertView addSubview:indicator];
    
    [_storage requestDownloadingPicsForCurrentTime];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -

- (void)onHTMLDidLoaded:(NSNotification *)aNotification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_loadingAlertView dismissWithClickedButtonIndex:0 animated:YES];
    });
}

- (void)onHTMLDidParse:(NSNotification *)aNotification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _storage.pics.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PicCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PicCell" forIndexPath:indexPath];
    WebPic *webPic = [_storage.pics objectAtIndex:indexPath.row];
    cell.webPic = webPic;
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PicCell *cell = (PicCell *)[collectionView cellForItemAtIndexPath:indexPath];
    WebPic *webPic = cell.webPic;
    if (webPic.failed) {
        [cell onRetryDownloadingPic];
        [_storage requestRetryDownloadingPic:webPic];
    }
}

@end
