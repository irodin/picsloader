//
//  AppConstants.m
//  PicsLoader
//
//  Created by admin on 19.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "AppConstants.h"

NSString *const PLHTMLDidLoadNotification = @"PLHTMLDidLoadNotification";
NSString *const PLHTMLDidParseNotification = @"PLHTMLDidParseNotification";
NSString *const PLPicDidLoadNotification = @"PLPicDidLoadNotification";
NSString *const PLPicDidUpdateNotification = @"PLPicDidUpdateNotification";