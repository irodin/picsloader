//
//  AppConstants.h
//  PicsLoader
//
//  Created by admin on 19.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const PLHTMLDidLoadNotification;
FOUNDATION_EXPORT NSString *const PLHTMLDidParseNotification;
FOUNDATION_EXPORT NSString *const PLPicDidLoadNotification;
FOUNDATION_EXPORT NSString *const PLPicDidUpdateNotification;
