//
//  AppDelegate.h
//  PicsLoader
//
//  Created by admin on 19.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
