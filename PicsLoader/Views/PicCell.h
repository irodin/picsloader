//
//  PicCell.h
//  PicsLoader
//
//  Created by admin on 21.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WebPic;

@interface PicCell : UICollectionViewCell

@property (nonatomic, strong) WebPic *webPic;

- (void)onRetryDownloadingPic;

@end
