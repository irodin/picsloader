//
//  PicCell.m
//  PicsLoader
//
//  Created by admin on 21.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "PicCell.h"
#import "AppConstants.h"
#import "WebPic.h"

@implementation PicCell {
    __weak IBOutlet UIImageView *_imageView;
    __weak IBOutlet UIActivityIndicatorView *_aiView;
    __weak IBOutlet UILabel *_errorLabel;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPicDidUpdate:) name:PLPicDidUpdateNotification object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setWebPic:(WebPic *)webPic
{
    if (_webPic != webPic) {
        _webPic = webPic;
        [self showPic];
    }
}

- (void)onPicDidUpdate:(NSNotification *)aNotification
{
    WebPic *webPic = (WebPic *)aNotification.object;
    if ([webPic.name isEqualToString:_webPic.name]) {
        _webPic = webPic;
        [self showPic];
    }
}

- (void)showPic
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_webPic.image) {
            _imageView.image = _webPic.image;
        }
        if (_webPic.image || _webPic.failed) {
            [_aiView stopAnimating];
        }
        _errorLabel.hidden = !_webPic.failed;
    });
}

- (void)onRetryDownloadingPic
{
    [_aiView startAnimating];
    _errorLabel.hidden = YES;
}

@end
