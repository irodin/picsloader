//
//  DownloadsManager.m
//  PicsLoader
//
//  Created by admin on 19.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "DownloadsManager.h"
#import "AppConstants.h"
#import "HTMLWithPics.h"
#import "WebPic.h"
#import "HTMLDownloadOperation.h"
#import "ImageDownloadOperation.h"

static NSInteger const kMaxConcurrentDownloadCount = NSOperationQueueDefaultMaxConcurrentOperationCount;

@interface DownloadsManager () <DownloadOperationDelegate>

@end

@implementation DownloadsManager {
    NSOperationQueue *_queue;
}

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    static DownloadsManager *sharedInstance = nil;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        _queue = [NSOperationQueue new];
        _queue.name = @"Download Queue";
        _queue.maxConcurrentOperationCount = kMaxConcurrentDownloadCount;
    }
    return self;
}

- (void)dealloc
{
    // should never be called as it's a singleton
}

#pragma mark -

- (void)downloadHTMLWithPicsForDate:(NSDate *)aDate
{
    [self cancellAllDownloads];
    
    // http://www.tenbyten.org/Data/global/YYYY/MM/DD/HH/

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy/MM/dd/HH/";
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    NSString *dateURLPart = [dateFormatter stringFromDate:aDate];
    NSString *baseURL = @"http://www.tenbyten.org/Data/global/";
    NSURL *htmlURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", baseURL, dateURLPart]];
    HTMLWithPics *html = [[HTMLWithPics alloc] initWithURL:htmlURL];
    
    HTMLDownloadOperation *op = [[HTMLDownloadOperation alloc] initWithHTML:html delegate:self];
    [_queue addOperation:op];
}

- (void)downloadPics:(NSArray *)webPics
{
    NSMutableArray *ops = [NSMutableArray arrayWithCapacity:webPics.count];
    for (WebPic *webPic in webPics) {
        ImageDownloadOperation *op = [[ImageDownloadOperation alloc] initWithWebPic:webPic delegate:self];
        [ops addObject:op];
    }
    
    [_queue addOperations:ops waitUntilFinished:NO];
}

- (void)setSuspended:(BOOL)isSuspended
{
    [_queue setSuspended:isSuspended];
}

- (void)cancellAllDownloads
{
    [_queue cancelAllOperations];
}

#pragma mark - DownloadOperationDelegate methods

- (void)downloadOperationDidFinish:(BaseDownloadOperation *)operation
{
    if ([operation isKindOfClass:[HTMLDownloadOperation class]]) {
        HTMLWithPics *html = [(HTMLDownloadOperation *)operation html];
        [[NSNotificationCenter defaultCenter] postNotificationName:PLHTMLDidLoadNotification object:html];
    } else if ([operation isKindOfClass:[ImageDownloadOperation class]]) {
        WebPic *webPic = [(ImageDownloadOperation *)operation webPic];
        [[NSNotificationCenter defaultCenter] postNotificationName:PLPicDidLoadNotification object:webPic];
    }
}

@end
