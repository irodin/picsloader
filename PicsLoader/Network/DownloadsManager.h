//
//  DownloadsManager.h
//  PicsLoader
//
//  Created by admin on 19.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DownloadsManager : NSObject

+ (instancetype)sharedInstance;

- (void)downloadHTMLWithPicsForDate:(NSDate *)aDate;
- (void)downloadPics:(NSArray *)webPics;
- (void)setSuspended:(BOOL)isSuspended;
- (void)cancellAllDownloads;

@end
