//
//  BaseDownloadOperation.m
//  PicsLoader
//
//  Created by admin on 25.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "BaseDownloadOperation.h"

@implementation BaseDownloadOperation

- (id)initWithDelegate:(id<DownloadOperationDelegate>)delegate
{
    if (self = [super init]) {
        _delegate = delegate;
        executing = NO;
        finished = NO;
    }
    return self;
}

- (BOOL)isConcurrent
{
    return YES;
}

- (BOOL)isExecuting
{
    return executing;
}

- (BOOL)isFinished
{
    return finished;
}

- (void)start
{
    if ([self isCancelled]) {
        [self willChangeValueForKey:@"isFinished"];
        finished = YES;
        [self didChangeValueForKey:@"isFinished"];
        return;
    }

    [self willChangeValueForKey:@"isExecuting"];
    [NSThread detachNewThreadSelector:@selector(main) toTarget:self withObject:nil];
    executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
}

- (void)main
{
    assert(NO); // must implement main in subclasses
}

- (void)completeOperation
{
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];
    executing = NO;
    finished = YES;
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

@end
