//
//  HTMLDownloadOperation.m
//  PicsLoader
//
//  Created by admin on 25.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "HTMLDownloadOperation.h"
#import "HTMLWithPics.h"

@implementation HTMLDownloadOperation

- (id)initWithHTML:(HTMLWithPics *)html delegate:(id<DownloadOperationDelegate>)delegate
{
    if (self = [super initWithDelegate:delegate]) {
        _html = html;
    }
    return self;
}

- (void)main
{
    @try {
        if (self.isCancelled) {
            return;
        }
        
        NSError *error;
        NSString *htmlSource = [NSString stringWithContentsOfURL:self.html.URL encoding:NSUTF8StringEncoding error:&error];
        
        if (self.isCancelled) {
            htmlSource = nil;
            return;
        }

        if (htmlSource) {
            self.html.source = htmlSource;
        }
        else {
            if (error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error loading %@", self.html.URL] message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                });
            }
            
            self.html.failed = YES;
        }
        
        htmlSource = nil;
        
        if (self.isCancelled) {
            return;
        }
        
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadOperationDidFinish:) withObject:self waitUntilDone:NO];
        
        [self completeOperation];
        
    } @catch(...) {
        // Do not rethrow exceptions.
    }
}

@end
