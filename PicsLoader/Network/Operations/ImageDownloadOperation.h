//
//  ImageDownloadOperation.h
//  PicsLoader
//
//  Created by admin on 25.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "BaseDownloadOperation.h"

@class WebPic;

@interface ImageDownloadOperation : BaseDownloadOperation

@property (nonatomic, strong) WebPic *webPic;

- (id)initWithWebPic:(WebPic *)webPic delegate:(id<DownloadOperationDelegate>)delegate;

@end
