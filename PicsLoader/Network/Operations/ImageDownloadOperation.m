//
//  ImageDownloadOperation.m
//  PicsLoader
//
//  Created by admin on 25.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "ImageDownloadOperation.h"
#import "WebPic.h"

@implementation ImageDownloadOperation

- (id)initWithWebPic:(WebPic *)webPic delegate:(id<DownloadOperationDelegate>)delegate
{
    if (self = [super initWithDelegate:delegate]) {
        _webPic = webPic;
    }
    return self;
}

- (void)main
{
    @try {
        if (self.isCancelled) {
            return;
        }
        
        NSData *imageData = [[NSData alloc] initWithContentsOfURL:self.webPic.URL];
        
        if (self.isCancelled) {
            imageData = nil;
            return;
        }
        
        // uncomment for testing re-downloading pic
        // (re-download by tapping "error" cell in collection view)
//        NSUInteger r = arc4random_uniform(2); // 0 or 1
//        if (r) {
//            imageData = nil;
//        }
        
        if (imageData) {
            UIImage *downloadedImage = [UIImage imageWithData:imageData];
            self.webPic.image = downloadedImage;
            self.webPic.failed = NO;
        } else {
            self.webPic.failed = YES;
        }
        
        imageData = nil;
        
        if (self.isCancelled) {
            return;
        }
        
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadOperationDidFinish:) withObject:self waitUntilDone:NO];
        
        [self completeOperation];
    } @catch(...) {
        // Do not rethrow exceptions.
    }
}

@end
