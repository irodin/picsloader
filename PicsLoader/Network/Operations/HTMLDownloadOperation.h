//
//  HTMLDownloadOperation.h
//  PicsLoader
//
//  Created by admin on 25.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import "BaseDownloadOperation.h"

@class HTMLWithPics;

@interface HTMLDownloadOperation : BaseDownloadOperation

@property (nonatomic, strong) HTMLWithPics *html;

- (id)initWithHTML:(HTMLWithPics *)html delegate:(id<DownloadOperationDelegate>)delegate;

@end
