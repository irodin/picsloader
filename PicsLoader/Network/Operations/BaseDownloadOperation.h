//
//  BaseDownloadOperation.h
//  PicsLoader
//
//  Created by admin on 25.11.14.
//  Copyright (c) 2014 irodin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BaseDownloadOperation;

@protocol DownloadOperationDelegate <NSObject>

- (void)downloadOperationDidFinish:(BaseDownloadOperation *)operation;

@end

@interface BaseDownloadOperation : NSOperation {
    BOOL executing;
    BOOL finished;
}

@property (nonatomic, weak) id<DownloadOperationDelegate> delegate;

- (id)initWithDelegate:(id<DownloadOperationDelegate>)delegate;
- (void)completeOperation;

@end
